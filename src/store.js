import  { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk'; //Nos permitira utilzar funciones asincronas
import reducer from './reducers'; //reducers/index

const store = createStore (
    reducer,
    compose( applyMiddleware(thunk),
        //Para poder utilizar la extension de redux en chrome
        
        typeof window === 'object' &&
            typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined' ?
                window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
    )
);

export default store;