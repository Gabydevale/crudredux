import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR
} from '../types';
import clienteAxios from '../config/axios';
import Swal from 'sweetalert2';

//Crear nuevos productos
export function crearNuevoProductoAction(producto){

     /* crearNuevoProductoAction->" esta funcion la llamamos en el 
     componente para que de esta forma los datos se pueden pasar a las acciones,
     utilizamos dispatch para ejecutar esta funcion o accion"*/
     
     return async (dispatch) => {             
        dispatch(agregarProducto());

        try {
            //insertar en la API
            await clienteAxios.post('/productos',producto);     //Esto lo pasamos a la api
           

            //Si todo sale bien, actualiza el state
            dispatch( agregarProductoExito( producto ));  //Esto lo pasamos al state
            
            //Alerta
            Swal.fire(
                'Correcto',
                'El producto se agregó correctamente',
                'success'
            )

        } catch (error) {
            console.log(error); //Mostramos el error para poder debuggear

            //si hay un error cambia el state
            dispatch( agregarProductoError(true) );

            //Alerta de error
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un eror, intenta de nuevo'
            })
        }
    }
}

const agregarProducto = () => ({ //Si definimos funcion en el action, se debe hacer lo mismo en el reducer
    type: AGREGAR_PRODUCTO
});

//si el producto se guarda en la base de datos
const agregarProductoExito = producto => (

    { //*********Action*************************************
       
        type: AGREGAR_PRODUCTO_EXITO,   
        payload: producto               //payload es lo que modifica el state

    } //***************************************************Action**************************************
)

//si hubo un error
const agregarProductoError = estado => ({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: estado
});

//Funcion que descarga los productos de la bd
export function obtenerProductosAction(){
    return async (dispatch) => {
        dispatch ( descargarProductos() );

        try {
            const respuesta = await clienteAxios.get('/productos');
            dispatch ( descargaProdcutosExitosa(respuesta.data) )
          
            
        } catch (error) {
            dispatch ( descargaProdcutosError() )
        }
    }
}

//Tambien es otra funcion 
const descargarProductos = () => ({
    type: COMENZAR_DESCARGA_PRODUCTOS,
    payload: true
});

const descargaProdcutosExitosa = productos => ({
    type: DESCARGA_PRODUCTOS_EXITO,
    payload: productos
});

const descargaProdcutosError = () => ({
    type: DESCARGA_PRODUCTOS_ERROR,
    payload: true
})