import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR
} from '../types';

//Cada reducer tiene su propio state
const initialState = {
    productos: [],
    error: null,
    loading: false

}

//Todo el reducer es un switch
export default function ( state = initialState, action) {
    switch(action.type) 
    {
        case COMENZAR_DESCARGA_PRODUCTOS:
        case AGREGAR_PRODUCTO:
            return {
                ...state,
                //loading: true //true porque se esta cargando el agregar nuevo producto
                loading: action.payload //pasamos el true via payload
            }
            
        case AGREGAR_PRODUCTO_EXITO:
            return{
                ...state,
                loading: false //falso porque ya se guardo en base de datos
            }    

        case DESCARGA_PRODUCTOS_ERROR:    
        case AGREGAR_PRODUCTO_ERROR:
            return{
                ...state,
                loading: false,
                error: action.payload
            } 
        
        case DESCARGA_PRODUCTOS_EXITO:
            return{
                ...state,
                loading: false,
                error: null,
                productos: action.payload
            }

        default:
            return state;
    }
}